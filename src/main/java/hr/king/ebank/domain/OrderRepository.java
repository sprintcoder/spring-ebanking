package hr.king.ebank.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderRepository extends CrudRepository<AccountOrder,Long>,
        PagingAndSortingRepository<AccountOrder,Long>{

    Page<AccountOrder> findByAccountUsername(String username, Pageable pageable);
    Page<AccountOrder> findByAccountUsernameAndStatus(String username, OrderStatus status,Pageable pageable);
    Page<AccountOrder> findByStatus(OrderStatus status,Pageable pageable);

}
