package hr.king.ebank.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AccountOrder {

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(nullable = false)
    private long id;

    @Column(nullable = false,length = 10)
    private String accountFrom;

    @Column(nullable = false,length = 10)
    private String accountTo;

    @Column(nullable = false)
    private int amount;

    @Enumerated
    @Column(nullable = false)
    private OrderStatus status;

    @ManyToOne
    @JoinColumn(name = "accountFrom",referencedColumnName = "accountNumber",updatable = false,insertable = false)
    private Account account;

    public OrderDto toOrderDto(){
        return new OrderDto(id,accountFrom,accountTo,amount,status);
    }

}
