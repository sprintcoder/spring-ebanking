package hr.king.ebank.domain;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class OrderPatchDto {

    @NotNull
    private long id;

    private OrderStatus status;

}
