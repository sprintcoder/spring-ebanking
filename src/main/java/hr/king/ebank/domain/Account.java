package hr.king.ebank.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "account")
public class Account implements Serializable
{

    @Id
    @GeneratedValue(strategy=IDENTITY)
    @Column(nullable = false)
    private long id;

    @Column(nullable = false,unique = true,length = 50)
    private String username;

    @Column(nullable = false,length = 70)
    private String password;

    @Column(nullable = false,unique = true,length = 10)
    private String accountNumber;

    @ColumnDefault("1000")
    @Column(nullable = false)
    private int accountAmount;


    public AccountDto toAccountDto(){

        return new AccountDto(id,username,password,accountNumber,accountAmount);

    }

}
