package hr.king.ebank.domain;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService{

    private final @NonNull OrderRepository orderRepository;
    private final @NonNull AccountRepository accountRepository;

    @Override
    public AccountOrder updateOrder(AccountOrder order, OrderPatchDto orderPatchDto) {
        order.setStatus(orderPatchDto.getStatus());

        if(order.getStatus() == OrderStatus.REJECTED){
            return this.orderRepository.save(order);
        }

        Account accountFrom = this.accountRepository.findByAccountNumber(order.getAccountFrom())
                .orElseThrow(() -> new AccountNotFoundException(order.getAccountFrom()));

        if(order.getAmount() <= accountFrom.getAccountAmount()){
            accountFrom.setAccountAmount(accountFrom.getAccountAmount() - order.getAmount());
            this.accountRepository.save(accountFrom);
            Account accountTo = this.accountRepository.findByAccountNumber(order.getAccountTo())
                    .orElseThrow(() -> new AccountNotFoundException(order.getAccountTo()));
            accountTo.setAccountAmount(accountTo.getAccountAmount() + order.getAmount());
            this.accountRepository.save(accountTo);
        }
        else {
            order.setStatus(OrderStatus.REJECTED);
        }
        return this.orderRepository.save(order);
    }

    @Override
    public AccountOrder createOrder(Account account, OrderDto orderDto) {
        AccountOrder order = orderDto.toOrder();
        order.setAccountFrom(account.getAccountNumber());
        if(order.getAmount() < 100){
            order.setStatus(OrderStatus.EXECUTED);
        }
        AccountOrder savedOrder = orderRepository.save(order);
        if(savedOrder.getId() % 10 == 0){
            savedOrder.setStatus(OrderStatus.SET);
            return orderRepository.save(savedOrder);
        }
        return savedOrder;
    }
}
