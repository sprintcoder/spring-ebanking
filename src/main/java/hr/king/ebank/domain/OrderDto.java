package hr.king.ebank.domain;


import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    private long id;

    private String accountFrom;

    @NotBlank
    private String accountTo;

    @NotNull
    @Positive
    private int amount;

    private OrderStatus status;

    public AccountOrder toOrder(){
        return new AccountOrder(0,null,accountTo,amount,OrderStatus.SET,null);
    }


}
