package hr.king.ebank.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class OrderPatchDtoValidator implements Validator{

    private final OrderRepository orderRepository;

    @Autowired
    public OrderPatchDtoValidator(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return OrderPatchDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        OrderPatchDto orderPatchDto = (OrderPatchDto) o;
        if(orderPatchDto.getStatus() == OrderStatus.SET){
            errors.rejectValue("status","NotSet",
                    "You can only execute or reject orders");
        }
        if(this.orderRepository.findById(orderPatchDto.getId()).isPresent()){
            AccountOrder order = this.orderRepository.findById(orderPatchDto.getId()).get();
            if(order.getStatus() != OrderStatus.SET){
                errors.rejectValue("status","NotSet","You can only update orders in set status");
            }
        }
    }

}
