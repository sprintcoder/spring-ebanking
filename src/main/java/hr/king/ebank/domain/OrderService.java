package hr.king.ebank.domain;

public interface OrderService {

    AccountOrder updateOrder(AccountOrder order, OrderPatchDto orderPatchDto);
    AccountOrder createOrder(Account account, OrderDto orderDto);

}
