package hr.king.ebank.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    private long id;

    @NotBlank
    @Size(min = 5,max = 50)
    private String username;

    @NotBlank
    @Size(min = 5,max = 50)
    private String password;

    @Size(min = 5,max = 10)
    @Pattern(regexp = "\\d+",message = "Must be numeric value")
    private String accountNumber;

    private int accountAmount;

    public Account toAccount(){
        return new Account(0,username,password,accountNumber,1000);
    }


    public AccountDto(String username, String password,String accountNumber)
    {
        this.username = username;
        this.password = password;
        this.accountNumber = accountNumber;
    }

}
