package hr.king.ebank.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class AccountDtoValidator implements Validator {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountDtoValidator(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return AccountDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        AccountDto accountDto = (AccountDto) o;
        if(accountRepository.findByUsername(accountDto.getUsername()).isPresent())
        {
            errors.rejectValue("username","UserUnique","User already exists");
        }
        if(accountRepository.findByAccountNumber(accountDto.getAccountNumber()).isPresent())
        {
            errors.rejectValue("accountNumber","AccountUnique","Account number already exists");
        }
    }
}
