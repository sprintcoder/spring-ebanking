package hr.king.ebank.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class OrderDtoValidator implements Validator {

    private final AccountRepository accountRepository;

    @Autowired
    public OrderDtoValidator(OrderRepository orderRepository, AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return OrderDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        OrderDto orderDto = (OrderDto) o;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        if (accountRepository.findByUsername(username).isPresent()){
            Account account = accountRepository.findByUsername(username).get();
            if (account.getAccountAmount() < orderDto.getAmount()){
                errors.reject("Amount on your account is less then ammount on your order.");
            }
        }
        if (!accountRepository.findByAccountNumber(orderDto.getAccountTo()).isPresent()){
            errors.rejectValue("accountTo","accountExists","To account doesn't exist.");
        }
    }
}
