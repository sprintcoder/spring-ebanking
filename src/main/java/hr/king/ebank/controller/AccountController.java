package hr.king.ebank.controller;


import hr.king.ebank.domain.Account;
import hr.king.ebank.domain.AccountDto;
import hr.king.ebank.domain.AccountDtoValidator;
import hr.king.ebank.domain.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.validation.Valid;

@RestController
@RequestMapping("/accounts")
public class AccountController
{

    private final AccountResourceAssembler accountResourceAssembler;
    private final AccountRepository accountRepository;
    private final AccountDtoValidator accountDtoValidator;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public AccountController(AccountResourceAssembler accountResourceAssembler, AccountRepository accountRepository, EntityManager em, AccountDtoValidator accountDtoValidator, PasswordEncoder passwordEncoder) {
        this.accountResourceAssembler = accountResourceAssembler;
        this.accountRepository = accountRepository;
        this.accountDtoValidator = accountDtoValidator;
        this.passwordEncoder = passwordEncoder;
    }

    @InitBinder("accountDto")
    public void setupBinder(WebDataBinder binder) {
        binder.addValidators(accountDtoValidator);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    Resource<AccountDto> create(@Valid @RequestBody AccountDto accountDto)
    {
        Account account = accountDto.toAccount();
        account.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        Account savedAccount = this.accountRepository.save(account);
        return this.accountResourceAssembler.toResource(savedAccount.toAccountDto());
    }

}