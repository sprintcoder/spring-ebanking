package hr.king.ebank.controller;


import hr.king.ebank.domain.Account;
import hr.king.ebank.domain.AccountDto;
import hr.king.ebank.domain.OrderDto;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import hr.king.ebank.controller.AccountResourceAssembler.AccountResource;

@Component
public class OrderResourceAssembler extends ResourceAssemblerSupport<OrderDto, OrderResourceAssembler.OrderResource>
{

    public OrderResourceAssembler()
    {
        super(OrderController.class, OrderResource.class);
    }

    @Override
    public OrderResource toResource(OrderDto orderDto)
    {
        OrderResource resource = createResourceWithId(orderDto.getId(), orderDto);
        return resource;
    }

    @Override
    protected OrderResource instantiateResource(OrderDto orderDto)
    {
        return new OrderResource(orderDto);
    }

    static class OrderResource extends Resource<OrderDto>
    {

        public OrderResource(OrderDto content)
        {
            super(content);
        }
    }

}
