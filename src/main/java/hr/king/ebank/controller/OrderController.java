package hr.king.ebank.controller;


import hr.king.ebank.controller.OrderResourceAssembler.OrderResource;
import hr.king.ebank.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orders")
public class OrderController {


    private final OrderDtoValidator orderDtoValidator;
    private final OrderPatchDtoValidator orderPatchDtoValidator;
    private final AccountRepository accountRepository;
    private final OrderRepository orderRepository;
    private final OrderResourceAssembler orderResourceAssembler;
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderDtoValidator orderDtoValidator, OrderPatchDtoValidator orderPatchDtoValidator, AccountRepository accountRepository, OrderRepository orderRepository, OrderResourceAssembler orderResourceAssembler, OrderService orderService) {
        this.orderDtoValidator = orderDtoValidator;
        this.orderPatchDtoValidator = orderPatchDtoValidator;
        this.accountRepository = accountRepository;
        this.orderRepository = orderRepository;
        this.orderResourceAssembler = orderResourceAssembler;
        this.orderService = orderService;
    }

    @InitBinder("orderDto")
    public void setupOrderBinder(WebDataBinder binder) {
        binder.addValidators(orderDtoValidator);
    }

    @InitBinder("orderPatchDto")
    public void setupOrderPatchBinder(WebDataBinder binder) {
        binder.addValidators(orderPatchDtoValidator);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    Resource<OrderDto> create(@Valid @RequestBody OrderDto orderDto, Principal principal)
    {
        return accountRepository
                .findByUsername(principal.getName())
                .map(account -> {
                    AccountOrder savedOrder = orderService.createOrder(account,orderDto);
                    return this.orderResourceAssembler.toResource(savedOrder.toOrderDto());
                })
                .orElseThrow(
                        () -> new UsernameNotFoundException("could not find the user " + principal.getName()));
    }

    @RequestMapping(value = "/myorders",method = RequestMethod.GET)
    PagedResources<OrderResource> myOrders(@PageableDefault(sort = "id") Pageable p, PagedResourcesAssembler<OrderDto> pagedAssembler,
                                             Principal principal, @RequestParam(value = "status", required = false) OrderStatus status){
        Page<AccountOrder> accountOrders;
        if(status == null){
            accountOrders = orderRepository.findByAccountUsername(principal.getName(),p);
        }
        else {
            accountOrders = orderRepository.findByAccountUsernameAndStatus(principal.getName(),status,p);
        }
        Page<OrderDto> pagedOrders = new PageImpl<>(accountOrders
                .stream()
                .map(AccountOrder::toOrderDto)
                .collect(Collectors.toList()),p,accountOrders.getTotalElements());
        return pagedAssembler.toResource(pagedOrders,orderResourceAssembler);
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    PagedResources<OrderResource> orders(@PageableDefault(sort = "id") Pageable p, PagedResourcesAssembler<OrderDto> pagedAssembler,
                                           Principal principal, @RequestParam(value = "status", required = false) OrderStatus status){
        Page<AccountOrder> accountOrders;
        if(status == null){
            accountOrders = orderRepository.findAll(p);
        }
        else {
            accountOrders = orderRepository.findByStatus(status,p);
        }
        Page<OrderDto> pagedOrders = new PageImpl<>(accountOrders
                .stream()
                .map(AccountOrder::toOrderDto)
                .collect(Collectors.toList()),p,accountOrders.getTotalElements());
        return pagedAssembler.toResource(pagedOrders,orderResourceAssembler);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    Resource<OrderDto> view(@PathVariable("id") long id)
    {
        return orderRepository
                .findById(id)
                .map(order -> this.orderResourceAssembler.toResource(order.toOrderDto()))
                .orElseThrow(
                        () -> new OrderNotFoundException(id));
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PATCH)
    Resource<OrderDto> update(@Valid @RequestBody OrderPatchDto orderPatchDto, @PathVariable("id") long id)
    {
        return orderRepository
                .findById(id)
                .map(order -> {
                    AccountOrder savedOrder = orderService.updateOrder(order,orderPatchDto);
                    return this.orderResourceAssembler.toResource(savedOrder.toOrderDto());
                })
                .orElseThrow(
                        () -> new OrderNotFoundException(id));
    }

}
