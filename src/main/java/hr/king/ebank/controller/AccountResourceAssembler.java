package hr.king.ebank.controller;


import hr.king.ebank.controller.AccountResourceAssembler.AccountResource;
import hr.king.ebank.domain.AccountDto;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class AccountResourceAssembler extends ResourceAssemblerSupport<AccountDto, AccountResource>
{

    public AccountResourceAssembler()
    {
        super(AccountController.class, AccountResource.class);
    }

    @Override
    public AccountResource toResource(AccountDto accountDto)
    {
        AccountResource resource = createResourceWithId(accountDto.getId(), accountDto);
        return resource;
    }

    @Override
    protected AccountResource instantiateResource(AccountDto accountDto)
    {
        return new AccountResource(accountDto);
    }

    static class AccountResource extends Resource<AccountDto>
    {

        public AccountResource(AccountDto content)
        {
            super(content);
        }
    }

}
