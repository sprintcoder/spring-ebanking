package hr.king.ebank;

import hr.king.ebank.domain.Account;
import hr.king.ebank.domain.AccountRepository;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountRepositoryTests
{

	@Autowired
	private AccountRepository accountRepository;

	@Test
	@Transactional
	@Sql("classpath:account-data.sql")
	public void givenSingleAccount_whenFindByUsername_thenShouldGetCorrectAccount()
	{
		Optional<Account> optionalAccount = accountRepository.findByUsername("Filip");
		if(optionalAccount.isPresent())
		{
			Account account = optionalAccount.get();
			assertThat(account.getUsername()).isEqualTo("Filip");
		}
	}

}
